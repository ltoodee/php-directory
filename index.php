<?php
$imgs = scandir(getcwd());

function isImage($img) {
	return strpos($img, ".jpg") > 0;
}

function hideImgFromDirList($img) {
	return !preg_match("/^\.+$/", $img);
}
?>

<html>
<head>
	<title>PHP directory listing</title>
	<style type="text/css">
		#dir, #images {
			display: none;
		}
		#showDir, #showImgs {
			cursor: pointer;
		}
		img {
			width: 23%;
		}
	</style>
</head>
<body>
<span id="showDir">Show directory</span> | <span id="showImgs">Show images</span>

<div id="dir">
	<code>
		<?php
		foreach($imgs as $img)
		{
			if(isImage($img))
			{
				print "<a class=\"dirLink\" imgSrc=\"$img\" href=\"#\">$img</a><br />" . PHP_EOL;
			}
			else
			{
				if(hideImgFromDirList($img))
				{
					print "$img<br />" . PHP_EOL;
				}
			}
			
		}
		?>
	</code>	
	<iframe id="tmpImg"></iframe>
</div>	
	
<div id="images">
<?php
foreach($imgs as $img)
{
	if(isImage($img))
	{
		print "<img src=\"$img\" />" . PHP_EOL;
	}
}
?>	
</div>
<script type="text/javascript">
	var showDir = document.getElementById("showDir"),
		showImgs = document.getElementById("showImgs"),
		dir = document.getElementById("dir"),
		imgs = document.getElementById("images"),
		dirLinks = document.getElementsByClassName("dirLink"),
		iframe = document.getElementById("tmpImg");
		
	showDir.addEventListener('click', showDirContent, false);
	showImgs.addEventListener('click', showImgsContent, false);
	
	for(var i = 0; i < dirLinks.length; i++) {
		var link = dirLinks[i];
		link.onclick = function () {
			updateIframeSource(link.getAttribute("imgSrc"));
		}
	}
	
	function updateIframeSource(source) {
		iframe.src = source;
	}
	
	function showDirContent() {
		dir.style.display = 'block';
		hideImgsContent();
	}
	
	function showImgsContent() {
		imgs.style.display = 'block';
		hideDirContent();
	}
	
	function hideDirContent() {
		dir.style.display = 'none';
	}
	
	function hideImgsContent() {
		imgs.style.display = 'none';
	}
</script>
</body>
</html>